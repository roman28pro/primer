import java.util.Scanner;

/**
 * Класс содержит метод
 * для выполнения математической операции и ввода данных
 *
 * @author Roman Kireev 17IT18
 */

public class Main {
    public static void main(String[] args) {
        Main calc = new Main();
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        double a = sc.nextDouble();
        System.out.println("Введите число на которое хотите умножить:");
        double b = sc.nextDouble();
        calc.Calc(a, b);
    }

    /**
     *Функция вычисления произведения
     *
     * @param a - первый множитель
     * @param b - второй множитель
     */

    public void Calc(double a, double b) {
        double answer = 0;
        for (int i = 0; i < b; i++) {
            answer+=a;
        }
        System.out.println("Ответ: " + answer);
    }

}